package sample.bean.friends;

public class Person {
	private final String name;
	private Person[] friends;
	
	public Person(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void friendsWith(Person... friends) {
		this.friends = friends;
	}

	public Person[] getFriends() {
		return friends;
	}
}
