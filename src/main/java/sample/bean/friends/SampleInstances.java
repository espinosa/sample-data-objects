package sample.bean.friends;

/**
 * Create examples of object graphs using friend relations between persons.
 * 
 * I had to made Person, the node class for the graph, partly mutable because
 * there is no simple way how to create a cyclic graph using immutables. See
 * http://stackoverflow.com/questions/4834513/how-to-model-cycles-between-immutable-class-instances
 * See
 * http://codereview.stackexchange.com/questions/127391/simple-builder-pattern-implementation-for-building-immutable-objects
 * 
 * @author Espinosa
 */
public class SampleInstances {
	
	/**
	 * Create a non-cyclic, tree graph of friends.
	 * @return a person representing root of the grap
	 */
	public static Person getPersonWithNonCyclicFrienship() {
        Person john = new Person("john");
        Person mary = new Person("mary");
        Person susan = new Person("susan");
        
        john.friendsWith(mary);
        mary.friendsWith(susan);
        	
        return john;
	}
	
	/**
	 * Create a cyclic graph of friends.
	 * @return a person representing root of the grap
	 */
	public static Person getPersonWithCyclicFrienship() {
        Person john = new Person("john");
        Person mary = new Person("mary");
        Person susan = new Person("susan");
        
        john.friendsWith(mary, susan);
        mary.friendsWith(susan, john);
        susan.friendsWith(john);

        return john;
	}
}
