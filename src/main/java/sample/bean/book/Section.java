package sample.bean.book;

import java.util.List;

/** test dummy class */
public class Section {
	private String name; 
	private String description; 
	private List<Paragraph> paragraphs;
	
	public Section() {
	}
	
	public Section(String name, String description, List<Paragraph> paragraphs) {
		this.name = name;
		this.description = description;
		this.paragraphs = paragraphs;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<Paragraph> getParagraphs() {
		return paragraphs;
	}
	public void setParagraphs(List<Paragraph> paragraphs) {
		this.paragraphs = paragraphs;
	}

	@Override
	public String toString() {
		return "Section [name=" + name + ", description=" + description + ", paragraphs=" + paragraphs + "]";
	}
}
