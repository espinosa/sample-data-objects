package sample.bean.book;

import java.util.Arrays;

public class SampleInstances {
	public static Book getSampleBook() {
		return new Book("History of Britain", "John O'Farrel", Arrays.asList(new Section[] {
			new Section("Section1", "no description", Arrays.asList(new Paragraph[] {
					new Paragraph(1, "text 1"),
					new Paragraph(2, "text 2"),
			})),
			new Section("Section2", "no description", Arrays.asList(new Paragraph[] {
					new Paragraph(1, "text 3"),
			})),
			new Section("Section3", "no description", Arrays.asList(new Paragraph[] {
					new Paragraph(1, "text 4"),
					new Paragraph(2, "text 5"),
					new Paragraph(3, "text 7"),
			})),
		}));
	}
}
