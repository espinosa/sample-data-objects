package sample.bean.book;

import java.util.List;

/**
 * Example of a typical Java bean featuring 1:N relations to other been. All
 * fields are R/W.
 */
public class Book {
	private String name;
	private String author;
	private List<Section> sections;

	public Book() {
	}

	public Book(String name, String author, List<Section> sections) {
		this.name = name;
		this.author = author;
		this.sections = sections;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public List<Section> getSections() {
		return sections;
	}

	public void setSections(List<Section> sections) {
		this.sections = sections;
	}

	@Override
	public String toString() {
		return "Book [name=" + name + ", author=" + author + ", sections=" + sections + "]";
	}
}
