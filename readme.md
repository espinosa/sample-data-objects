Idea is to slowly build a repository full of interesting examples of various data objects to be used in unit tests, 
to test graph traversal, rendering of complex classes, introspection, serialization to different formats, like Json or XML, to test technologies like JAXB or JPA.
Idea is to have not only definition of data models bul also a working instantiable examples.
Importing test classes and ready to use examples should free unit tests of the target ptoject smaller, clearer and focused on their domain.

So far I am on the very beggining. It started as extracted couple of sample model classes from my another project - logback-layout-json.
I felt they polute the unit test sources with rather distractful buch of unrelated classes.   